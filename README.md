# PHP Examples

This repository contains examples that PHP developers will find useful. The examples will focus on well designed code, development with containers, and best practice devops, deployment, and monitoring principles.

## Examples

- [ ] [Docker & Kubernetes](docker-and-kubernetes)
- [ ] [Kubernetes](kubernetes)
